import React from 'react';
import { Link } from "react-router-dom";
import { motion } from 'framer-motion';

// Utilites
import NumberTicker from '../NumberTicker'

const Announcement : React.FC = () => {

    return <>
        <motion.div id="announcement-page" className="bg-blue-gradient text-center position-relative h-100"
            style={{ marginLeft: '0px', marginRight: '0px' }} 
            animate={{ marginLeft: '77px', marginRight: '77px'}} 
            transition={{ duration: 3, ease: "easeInOut" }}
        >

            <motion.div className="apple-logo" 
                style={{ scale: 0.5, opacity: 0, translateX: '-50%', translateY: '-50%'  }} 
                animate={{ scale: 1, opacity: 1 }} 
                transition={{ duration: 3, ease: "easeInOut" }}
            >
                <i className="fab fa-apple"></i>                    
            </motion.div>
            
            <div className="d-flex justify-content-center align-items-center h-100">
                
                <div id="announcement-text" className="text-center">

                    <motion.p style={{ y: '50px', scale: 3, opacity: 0 }} animate={{ y: '0px', scale: 1, opacity: 1 }} transition={{ duration: 3, ease: "easeInOut" }}>
                    New Products Coming This Summer
                    </motion.p>

                    <motion.div id="launchYear" style={{ opacity: 0 }} animate={{ opacity: 1 }} transition={{ duration: 3, ease: "easeIn" }}>
                        <NumberTicker
                            style={{alignItems: 'center'}}
                            textStyle={{ fontWeight: '700'}}
                            number="2019"
                            textSize={40}
                            duration={3000}
                        />
                    </motion.div>

                    <motion.div style={{ opacity: 0, y: '50px' }} animate={{ opacity: 1, y: '0' }} transition={{ duration: 1, ease: "easeInOut", delay: 3 }}>
                        <Link to="/" className="btn btn-dark btn-animate">
                            <span>Enter<i className="far fa-arrow-right"></i></span>
                        </Link>
                    </motion.div>

                </div>

            </div>

        </motion.div>
        
    </>
}

export default Announcement;