import React from 'react';
import { Link } from "react-router-dom";
import { motion } from 'framer-motion';

// Utilities
import IconNavigation from '../IconNavigation';

const Home : React.FC = () => {

    // Transition properties used for animations
    const standardTransition = { duration: 1, ease: "easeInOut" };

    return <>
        <div id="home-page" className="container-wrapper bg-grey-gradient text-center h-100">
                        
            <div className="apple-logo" >
                <i className="fab fa-apple"></i>                    
            </div>
            
            <div className="d-flex justify-content-center align-items-center h-100">

                <div id="homepage-text" className="text-center">

                    <motion.div style={{ y: '-50px', opacity: 0 }} animate={{ y: '0', opacity: 1 }} transition={ standardTransition }>
                        <h1 className="mb-4">Welcome to Apple</h1>
                    </motion.div>

                    <motion.div style={{ y: '40px', opacity: 0 }} animate={{ y: '0', opacity: 1 }} transition={ standardTransition }>
                        <div className="mb-5">
                            <Link to="/iphone">See our Products</Link>
                        </div>

                        <IconNavigation orientation="horizontal" color="light" />
                    </motion.div>

                </div>

            </div>

        </div>
    </>
}

export default Home;
