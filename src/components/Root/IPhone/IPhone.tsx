import React from 'react';
import { motion } from 'framer-motion';

// Utilities
import ProductDate from '../ProductDate';
import IconNavigation from '../IconNavigation';

// Images
import iphoneFront from '../../../assets/img/iphone-front.png';
import iphoneBack from '../../../assets/img/iphone-back.png';
import iphoneThumbFont from '../../../assets/img/iphone-front-thumbnail.png';
import iphoneThumbBack from '../../../assets/img/iphone-back-thumbnail.png';

const IPhone : React.FC = () => {

    // Transition properties used for animations
    const standardTransition = { duration: 1, ease: "easeInOut" };
    
    return <> 
        <div id="iphone-page" className="container-wrapper product-page">

            {/* Icon Navigation */}
            <div className="vertical-navigation">
                <IconNavigation orientation="vertical" color="light" />
            </div>

            <div className="container-fluid">

                {/* iPhone Banner */}
                <motion.div className="row bg-grey-gradient" style={{ y: '100px', opacity: 0 }} animate={{ y: '0', opacity: 1 }} transition={ standardTransition }>
                    <div className="col-12">
                        
                        <div className="banner">
                            <div className="row">

                                <div className="banner-logo">
                                    <i className="fab fa-apple"></i>
                                </div>

                                {/* Banner Text */}
                                <div className="col-lg-6">
                                    <div className="banner-text">
                                        <div className="h5 text-secondary mb-4">iPhone</div>
                                        <h1>The ultimate iPhone</h1>
                                        <p className="text-light">The future is here Join the iPhone Upgrade Program to get the latest iPhone - NOW!</p>
                                        <div className="text-secondary"><small>Starts shipping <ProductDate product="iphone" /></small></div>
                                    </div>
                                </div>

                                {/* iPhone Images */}
                                <div className="col-lg-6 text-center">
                                    <div className="tab-content">

                                        <div id="iphone-front" className="tab-pane fade show active" role="tabpanel">
                                            <img src={ iphoneFront } alt="iPhone - Front" className="product-image" />
                                        </div>
                                        <div id="iphone-back" className="tab-pane fade" role="tabpanel">
                                            <img src={ iphoneBack } alt="iPhone - Back" className="product-image" />
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </motion.div>

                <motion.div className="row" style={{ x: '-100px', opacity: 0 }} animate={{ x: '0', opacity: 1 }} transition={ standardTransition }>
                    <div className="col-12">

                        <div className="product-information">
                            <div className="row">
                                <div className="col-xl-4 text-center text-lg-left">
                                    <h2 className="text-light mb-0">From $699</h2>
                                    <a href="/iPhone" className="text-secondary"><small>Buy Now ></small></a>
                                </div>
                                
                                <div className="col-xl-4 col-lg-6 ml-lg-auto ml-xl-0">

                                    {/* Phone Buttons */}
                                    <ul id="phone-toggles" className="nav py-4 pt-xl-0 justify-content-center justify-content-lg-center justify-content-xl-end" role="tablist">
                                        <li>
                                            <button id="iphone-front-tab" className="phone-toggle active" data-toggle="tab" data-target="#iphone-front" role="tab">
                                                <img src={ iphoneThumbFont } alt="iPhone - Front" />
                                            </button>
                                        </li>
                                        <li>
                                            <button id="iphont-back-tab" className="phone-toggle" data-toggle="tab" data-target="#iphone-back" role="tab">
                                                <img src={ iphoneThumbBack } alt="iPhone - Back" />
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </motion.div>

            </div>
        </div>
    </>
}

export default IPhone;
