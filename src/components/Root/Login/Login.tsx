import React from 'react';
import { Link, Redirect } from "react-router-dom";
import { motion } from 'framer-motion';

interface loginForm{
  loggedIn: boolean
  formFields: {
    email: String, 
    password: String
  }, 
  emailError: String, 
  passwordError: String
}

class Login extends React.Component<{}, loginForm>{

  state = {
    loggedIn: false,
    formFields: {
      email: "", 
      password: ""
    },
    emailError: "", 
    passwordError: ""
  }

  // Checks if user is currently logged in
  componentWillMount(){
    const authStatus = sessionStorage.getItem('loggedIn') || '0';
    this.setState({ loggedIn: Boolean(parseInt(authStatus)) });
  }

  // Function used to validate login form
  validate = () => {

    const { formFields } = this.state;

    // Clear current errors
    this.setState({ emailError: "", passwordError: "" });

    // Return value
    var isValid = true;
    
    // Email field required
    if(this.isEmpty(formFields.email)){
      this.setState({ emailError: "Required." });
      isValid = false;
    }
    // Email must be valid
    else{
      if(!this.isValidEmail(formFields.email)){
        this.setState({ emailError: "Email Invalid." });
        isValid = false;
      }
    }

    // Password field required
    if(this.isEmpty(formFields.password)){
      this.setState({ passwordError: "Required." });
      isValid = false;
    }

    return isValid;

  }

  // Checks if a String is Empty
  isEmpty = (val: String) =>{
    if(val === ""){
      return true;
    }
    return false;
  }

  // Checks if String is a valid email
  isValidEmail = (val: String) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(val).toLowerCase());
  }

  // Updates Component State
  updateField = (e) => {
    
    // Get field name and value
    const { formFields } = { ...this.state };
    const currentState = formFields;
    const { name, value } = e.target;
    currentState[name] = value;

    // Update state object
    this.setState({ formFields: currentState });
  }

  // Logs in user
  login = (e) => {
    e.preventDefault();
    
    // Validate form
    const isValid = this.validate(); 
    
    if(isValid){
      
      // Get registered users
      var registeredUsers: any = sessionStorage.getItem('registeredUsers') || [];
      registeredUsers = JSON.parse(registeredUsers);

      // Iterate through users
      registeredUsers.map(user => {

        // Check if user is registered
        if(user.email === this.state.formFields.email){
          
          // Check if password is correct
          if(user.password === this.state.formFields.password){
            
            // Set session variable for logged in 
            sessionStorage.setItem('loggedIn', '1');

            // Set session variable for current user
            sessionStorage.setItem('currentUser', JSON.stringify(user));

            this.setState({ loggedIn: true });

            return;
          }
        }
      });

    }
  }

  render(){

    const { loggedIn, formFields, emailError, passwordError } = this.state;

    const standardTransition = { duration: 1, ease: "easeInOut" };

    if(!loggedIn){
      return (
        <div id="login-form" className="bg-blue-gradient auth-form vh-100">

          <div className="container h-100">
            <div className="row justify-content-center h-100">
              <div className="col-sm-9 col-md-6 col-xl-5 my-auto">

                {/* White Box */}
                <motion.div className="card py-4 px-3" style={{ y: '50px', opacity: 0 }} animate={{ y: '0', opacity: 1 }} transition={ standardTransition }>
                  <div className="card-body">

                    <h1 className="text-center pb-3 h3">Sign-In</h1>

                    <form onSubmit={this.login}>

                      {/* Email */}
                      <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input 
                          type="text" 
                          name="email" 
                          id="email" 
                          className="form-control" 
                          value={ formFields.email } 
                          onChange={ this.updateField } 
                        />
                        <div className="badge badge-danger">
                          { emailError }
                        </div>
                      </div>

                      {/* Password */}
                      <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input 
                          type="password" 
                          name="password" 
                          id="password" 
                          className="form-control" 
                          value={ formFields.password } 
                          onChange={ this.updateField } 
                        />
                        <div className="badge badge-danger">
                          { passwordError }
                        </div>
                      </div>

                      {/* Submit Button */}
                      <div className="text-center">
                      <button type="submit" className="btn btn-dark btn-animate">
                        <span>Sign-In<i className="far fa-arrow-right"></i></span>
                      </button>
                      </div>

                    </form>

                  </div>
                </motion.div>   
                     
                <div className="text-right mt-3">
                  <small>
                    Not registered? <Link to="/register">Sign-up</Link>
                  </small>
                </div>
              </div>

            </div>
          </div>

        </div>
      )
    }
    else{
      return(
        <Redirect to="/announcement" />
      );
    }
  }
  
}

export default Login;
