import React from 'react';
import { motion } from 'framer-motion';

// Utilities
import ProductDate from '../ProductDate';
import IconNavigation from '../IconNavigation';

// Images
import macbook from '../../../assets/img/macbook-pro.png';
import macProducts from '../../../assets/img/mac-products.png';

const MacbookPro : React.FC = () => {

    // Transition properties used for animations
    const standardTransition = { duration: 1, ease: "easeInOut" };
    
    return <> 
        <div id="macbook-page" className="product-page">
                
            <div className="container-wrapper">
                {/* Icon Navigation */}
                <div className="vertical-navigation">
                    <IconNavigation orientation="vertical" color="dark" />
                </div>

                <div className="container-fluid">

                    {/* Macbook Banner */}
                    <div className="row bg-dark">
                        <div className="col-12">
                            
                            <div className="banner">
                                <div className="row">

                                    {/* Banner Text */}
                                    <div className="col-lg-6">
                                        <div className="banner-text">
                                            <motion.div style={{ x: "-100px", opacity: 0 }} animate={{ x: "0px", opacity: 1 }} transition={ standardTransition }>
                                                <div className="h5 text-primary m-0">MacBook Pro</div>
                                                <div className="text-secondary"><small>Starts shipping <ProductDate product="mackbook" /></small></div>
                                                <h1 className="text-white mb-5">
                                                    <span>More power.</span>
                                                    <span>More pro.</span>
                                                </h1>
                                            </motion.div>
                                            <motion.div id="macbook-details" className="d-flex" style={{ x: "100px", opacity: 0 }} animate={{ x: "0px", opacity: 0.44}} transition={ standardTransition }>
                                                <div className="text-grey">
                                                    <h2 className="m-0">8-core</h2>
                                                    <p><small>Intel processor</small></p>
                                                </div>
                                                <div className="text-grey">
                                                    <h2 className="m-0">32GB</h2>
                                                    <p><small>Memory</small></p>
                                                </div>
                                            </motion.div>
                                        </div>
                                    </div>

                                    {/* Macbook Image */}
                                    <div className="col-lg-6 text-center">
                                        <img src={ macbook } alt="Macbook Pro" id="macbook-pro" className="product-image" />
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>

                    <motion.div className="row" style={{ y: "50px", opacity: 0 }} animate={{ y: "0px", opacity: 1}} transition={ standardTransition }>
                        <div className="col-12">

                            <div className="product-information">
                                <div className="row">

                                    <div className="col-lg-2 no-gutters d-none d-lg-inline-block">
                                        <a href="/macbook-pro" className="text-primary"><small>Buy Now ></small></a>
                                    </div>

                                    <div className="col-lg-2 no-gutters d-block d-lg-none">
                                        <a href="/macbook-pro" className="btn btn-animate btn-block btn-secondary">
                                            <span>Buy Now<i className="far fa-arrow-right"></i></span>
                                        </a>
                                    </div>

                                    <div className="col-lg-5 no-gutters">
                                        <img src={ macProducts } alt="Mac Products" className="w-100" />
                                    </div>

                                    {/* Subscribe Form */}
                                    <div className="col-lg-5 no-gutters">
                                        <div id="subscribe-form" className="col-12">
                                            <h2>Subscribe Now</h2>
                                            <form action="" className="row no-gutters">
                                                <div className="col-8 col-lg-12 col-xl-8">
                                                    <div className="form-group mb-0 pr-2 pr-lg-0 pr-xl-2">
                                                        <input type="text" className="form-control" placeholder="Enter the email address" />
                                                    </div> 
                                                </div>
                                                <div className="col-4 col-lg-12 col-xl-4">
                                                    <button type="submit" className="btn btn-block btn-dark">
                                                        Subscribe
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </motion.div>

                </div>
            </div>

        </div>
    </>;
}

export default MacbookPro;
