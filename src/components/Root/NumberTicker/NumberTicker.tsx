import React from 'react';
import {Animated, Easing, StyleSheet, Text, View} from "react-native";

interface numberTickerPropTypes{
    style: Object,  
    textSize: number, 
    textStyle: Object,
    number: string, 
    duration: number,
}

interface textTickerPropTypes{
    textSize: number,
    textStyle: Object,
    targetNumber: number,
    duration: number,
};

interface textTickerStateTypes{ 
    animatedValue: number,
    isAnimating: boolean,
    number: number,
    numberList: Array<{id: number}>
};

const NumberTicker : React.FC<numberTickerPropTypes> = ({style, textSize = 35, textStyle, number, duration}) => {

    const mapToDigits = () => {
        return (number).split('').map((data) => {
            return (
                <TextTicker
                    key={ data }
                    textSize={ textSize }
                    textStyle={ textStyle }
                    targetNumber={ parseInt(data) }
                    duration={ duration }
                />
            );
        })
    };

    return (
        <View style={style}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                {mapToDigits()}
            </View>
        </View>
    );
};

class TextTicker extends React.Component<textTickerPropTypes, textTickerStateTypes> {

    constructor(props) {
        super(props);
        this.state = {
            animatedValue: new Animated.Value(0),
            isAnimating: true,
            number: 1,
            numberList: []
        };
        const {targetNumber} = this.props;

        if (this.props.targetNumber > 5) {
            for (let i = 0; i <= targetNumber; i++) {
                this.state.numberList.push({id: i});
            }
        } else {
            for (let i = 9; i >= targetNumber; i--) {
                this.state.numberList.push({id: i});
            }
        }
    }

    componentDidMount() {
        this.startAnimation();
    }

    startAnimation = () => {
        const {animatedValue} = this.state;
        Animated.timing(animatedValue, {
            toValue: 1,
            duration: this.props.duration,
            easing: Easing.inOut(Easing.cubic),
            useNativeDriver: true,
        }).start();
    };

    getInterpolatedVal = (val) => {
        return val.interpolate({
            inputRange: [0, 1],
            outputRange: [this.props.textSize * this.state.numberList.length, this.props.textSize*0.2],
            extrapolate: 'clamp',
        });
    };


    renderNumbers = (styles) => {
        return this.state.numberList.map((data) => {
            return (
                <Text key={data.id} style={[this.props.textStyle, styles.text]}>{data.id}</Text>
            )
        });
    };

    render() {
        const {animatedValue} = this.state;
        const styles = generateStyles(this.props.textSize);

        return (
            <View style={styles.container}>
                <Animated.View style={{
                    transform: [{
                        translateY: this.getInterpolatedVal(animatedValue)
                    }]
                }}>
                    {this.renderNumbers(styles)}
                </Animated.View>
            </View>
        );
    }
}

const generateStyles = (textSize) => StyleSheet.create({
    container: {
        width: textSize * 0.62,
        height: textSize,
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    text: {
        fontSize: textSize,
        lineHeight: textSize + 10,
    },
});

export default NumberTicker;