import React from 'react';
import { Route, Switch } from "react-router-dom";
import Navigation from '../Navigation';

// Pages
import Announcement from '../Announcement';
import Home from '../Home';
import IPhone from '../IPhone';
import MacbookPro from '../MacbookPro';
import Watch from '../Watch';

const PrivatePageLayout : React.FC = () => {
    
    return <> 
        <div className="d-flex min-vh-100">
            <div className="w-100">
                <div className="d-flex flex-column h-100">
                    <div className="d-flex">
                        <div className="w-100">
                            <Navigation />
                        </div>
                    </div>
                    <div className="d-flex flex-grow-1">
                        <div className="w-100">
                            <Switch>
                                <Route component={ Announcement } exact path="/announcement" />
                                <Route component={ Home } exact path="/" />
                                <Route component={ IPhone } path="/iphone" />
                                <Route component={ MacbookPro } path="/macbook-pro" />
                                <Route component={ Watch } path="/watch" />
                            </Switch>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </>;
}

export default PrivatePageLayout;
