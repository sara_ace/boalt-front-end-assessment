import React from 'react';
import { Redirect, Route } from "react-router-dom";

class PrivateRoute extends React.Component<{component: any, exact: boolean, path: String}, {}>{

    state = {
        loggedIn: false
    }

    componentWillMount(){
        const authStatus = sessionStorage.getItem('loggedIn') || '0';
        this.setState({ loggedIn: Boolean(parseInt(authStatus)) });
    }

    render(){
        if(!this.state.loggedIn){
            return (
                <Redirect to='/login' />
            )
        }
        else {
            return (
                <div>
                    <Route component={ this.props.component } exact={ this.props.exact } path={ this.props.path } />
                </div>
            )
        }
    }
  
}

export default PrivateRoute;