import React from 'react';

class ProductDate extends React.Component<{product: string}, {isLoaded: Boolean, date: String}>{

    state = {
        isLoaded: false,
        date: ""
    }

    // Fetch Date for product from API
    componentDidMount() {
        fetch("https://cors-anywhere.herokuapp.com/https://boalt-interview.herokuapp.com/api/shipping-dates")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true, 
                        date: result[ this.props.product ]
                    })
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        date: 'Cannot reach resource.'
                    });
                }
            )
      }

    render(){

        const{ date } = this.state;

        return (
            <div className="d-inline">
                { date }
            </div>
        );
    }
}

export default ProductDate;
