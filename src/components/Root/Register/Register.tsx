import React from 'react';
import { Link } from "react-router-dom";
import { motion } from 'framer-motion';
import { dimgray } from 'color-name';

interface registrationForm{
  registered: boolean,
  loggedIn: boolean,
  formFields: {
    name: String, 
    email: String, 
    password: String
  }, 
  nameError: String, 
  emailError: String, 
  passwordError: String
}

class Register extends React.Component<{}, registrationForm>{

  state = {
    registered: false,
    loggedIn: false,
    formFields: {
      name: "", 
      email: "", 
      password: ""
    },
    nameError: "", 
    emailError: "", 
    passwordError: ""
  }

  // Checks if user is currently logged in
  componentWillMount(){
    const authStatus = sessionStorage.getItem('loggedIn') || '0';
    this.setState({ loggedIn: Boolean(parseInt(authStatus)) })
  }

  validate = () => {

    const { formFields } = this.state;

    // Clear current errors
    this.setState({ nameError: "", emailError: "", passwordError: "" });

    // Return value
    var isValid = true;

    // Name field required
    if(this.isEmpty(formFields.name)){
      this.setState({ nameError: "Required." });
      isValid = false;
    }
    
    // Email field required
    if(this.isEmpty(formFields.email)){
      this.setState({ emailError: "Required." });
      isValid = false;
    }
    // Email must be valid
    else{
      if(!this.isValidEmail(formFields.email)){
        this.setState({ emailError: "Email Invalid." });
        isValid = false;
      }
    }

    // Password field required
    if(this.isEmpty(formFields.password)){
      this.setState({ passwordError: "Required." });
      isValid = false;
    }

    return isValid;

  }

  // Checks if a String is Empty
  isEmpty = (val: String) =>{
    if(val === ""){
      return true;
    }
    return false;
  }

  // Checks if String is a valid email
  isValidEmail = (val: String) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(val).toLowerCase());
  }

  // Updates Component State
  updateField = (e) => {
    
    // Get field name and value
    const { formFields } = { ...this.state };
    const currentState = formFields;
    const { name, value } = e.target;
    currentState[name] = value;

    // Update state object
    this.setState({ formFields: currentState });
  }

  // Registers user
  register = (e) => {
    
    e.preventDefault();
    
    const isValid = this.validate(); 

    if(isValid){
      // Register new user from form fields
      const newUser = this.state.formFields;

      // Get existing registered users from session
      var registeredUsers: any = sessionStorage.getItem('registeredUsers') || '[]';
      registeredUsers = JSON.parse(registeredUsers);

      // Push new user onto array
      registeredUsers.push(newUser);

      // Set session variable
      registeredUsers = JSON.stringify(registeredUsers);
      sessionStorage.setItem('registeredUsers', registeredUsers);

      // Update State
      this.setState({ registered: true })
    }

  }

  // Logs out user
  logout = (e) => {
    e.preventDefault(); 

    sessionStorage.setItem('loggedIn', '0');

    this.setState({ loggedIn: false });

  }

  render(){

    const { registered, loggedIn, formFields, nameError, emailError, passwordError } = this.state;
    
    const standardTransition = { duration: 1, ease: "easeInOut" };

    var successMessage;
    if(registered){
      successMessage = <div className="alert alert-success"><i className="fa fa-check"></i>&nbsp;Registered Successfully</div>;
    }
    
    if(!loggedIn){
      return (
      
        <div id="registration-form" className="bg-blue-gradient auth-form vh-100">
  
          <div className="container h-100">
            <div className="row justify-content-center h-100">
              <div className="col-sm-9 col-md-6 col-xl-5 my-auto">
  
                {/* White Box */}
                <motion.div className="card py-4 px-3" style={{ y: '-50px', opacity: 0 }} animate={{ y: '0', opacity: 1 }} transition={ standardTransition }>
                  <div className="card-body">
  
                    <h1 className="text-center pb-3 h3">Sign-Up</h1>

                    {/* Registered Successfully */}
                    { successMessage }
  
                    {/* Form */}
                    <form onSubmit={this.register}>
  
                      {/* Full Name */}
                      <div className="form-group">
                        <label htmlFor="name">Full Name</label>
                        <input 
                          type="text" 
                          name="name" 
                          id="name" 
                          className="form-control" 
                          value={ formFields.name } 
                          onChange={ this.updateField } 
                        />
                        <div className="badge badge-danger">
                          { nameError }
                        </div>
                      </div>
  
                      {/* Email */}
                      <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input 
                          type="text" 
                          name="email" 
                          id="email" 
                          className="form-control" 
                          value={ formFields.email } 
                          onChange={ this.updateField } 
                        />
                        <div className="badge badge-danger">
                          { emailError }
                        </div>
                      </div>
  
                      {/* Password */}
                      <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input 
                          type="password" 
                          name="password" 
                          id="password" 
                          className="form-control" 
                          value={ formFields.password } 
                          onChange={ this.updateField } 
                        />
                        <div className="badge badge-danger">
                          { passwordError }
                        </div>
                      </div>
  
                      {/* Submit Button */}
                      <div className="text-center">
                        <button type="submit" className="btn btn-dark btn-animate">
                          <span>Sign-Up<i className="far fa-arrow-right"></i></span>
                        </button>
                      </div>
  
                    </form>
  
                  </div>
                </motion.div>
  
                {/* Link to Login */}
                <div className="text-left mt-3">
                  <small>
                    Already registered? <Link to="/login">Sign-in</Link>
                  </small>
                </div>
  
              </div>
            </div>
          </div>
  
        </div>
      );
    }
    else{
      return(
        <div className="auth-form bg-blue-gradient vh-100">

          <div className="container h-100">
            <div className="row justify-content-center h-100">
              <div className="col-sm-9 col-md-6 col-xl-5 my-auto">

                <div className="card py-4 px-3">
                  <div className="card-body">

                    <h1 className="text-center pb-3 h3">Already Signed-In</h1>
                    
                    {/* Sign Out Button*/}
                    <div className="text-center">
                      <button type="button" className="btn btn-dark btn-animate" onClick={ this.logout }>
                        <span>Sign-Out<i className="far fa-arrow-right"></i></span>
                      </button>
                    </div>

                  </div>
                </div>

              </div>
            </div>
          </div>

        </div>
      );
    }
  }
  
}

export default Register;
