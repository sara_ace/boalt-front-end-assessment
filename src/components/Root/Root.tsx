import React from 'react';
import { Route, Switch } from 'react-router-dom';

// Utilities
import PrivateRoute from './PrivateRoute';
import PrivatePageLayout from './PrivatePageLayout';

// Pages
import Login from './Login';
import Register from './Register';

function Root() {
  return (
    <div>
      <Switch>
        <PrivateRoute component={ PrivatePageLayout } exact path="/announcement" />
        <PrivateRoute component={ PrivatePageLayout } exact path="/" />
        <PrivateRoute component={ PrivatePageLayout } exact path="/iphone" />
        <PrivateRoute component={ PrivatePageLayout } exact path="/macbook-pro" />
        <PrivateRoute component={ PrivatePageLayout } exact path="/watch" />
        <Route component={ Login } path="/login" />
        <Route component={ Register } path="/register" />
      </Switch>
    </div>
  );
}

export default Root;
