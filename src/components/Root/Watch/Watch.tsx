import React from 'react';
import { motion } from 'framer-motion';

// Utilities
import ProductDate from '../ProductDate';
import IconNavigation from '../IconNavigation';

// Images
import blackWatch from '../../../assets/img/watch-black.png';
import whiteWatch from '../../../assets/img/watch-white.png';

const Watch : React.FC = () => {

    // Transition properties used for animations
    const standardTransition = { duration: 1, ease: "easeInOut" };
    
    return <>
        <div id="watch-page" className="container-wrapper product-page">

            {/* Icon Navigation */}
            <div className="vertical-navigation">
                <IconNavigation orientation="vertical" color="light" />
            </div>
            
            <div className="container-fluid">
            
                {/* Apple Watch Banner */}
                <motion.div className="row bg-grey-gradient" style={{ x: '-100px', opacity: 0 }} animate={{ x: '0', opacity: 1 }} transition={ standardTransition }>
                    <div className="col-12">
                        
                        <div className="banner">
                            <div className="row">

                                <div className="banner-logo">
                                    <i className="fab fa-apple"></i>
                                </div>

                                <div className="col-xl-6 col-lg-7">
                                    <div className="banner-text">
                                        <div className="h5 text-secondary mb-4">Apple Watch</div>
                                        <h1>Change starts within.</h1>
                                        <p className="text-grey">Apple Watch Series 4. Fundamentally redesigned and re-engineered to help you be even more active, healthy, and connected.</p>
                                        <p className="text-secondary"><small>Starts shipping <ProductDate product="watch" /></small></p>
                                    </div>
                                </div>

                                {/* Watch Images */}
                                <div className="col-xl-6 col-lg-5 align-self-center" >
                                    <div className="tab-content text-center text-lg-right">

                                        <div id="white-watch" className="tab-pane fade" role="tabpanel">
                                            <img src={ whiteWatch } alt="Apple Watch - White Band" className="product-image" />
                                        </div>
                                        <div id="black-watch" className="tab-pane fade show active" role="tabpanel">
                                            <img src={ blackWatch } alt="Apple Watch - Black Band" className="product-image" />
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </motion.div>

                <motion.div className="row align-self-end" style={{ y: '50px', opacity: 0 }} animate={{ y: '0', opacity: 1 }} transition={ standardTransition }>
                    <div className="col-12">

                        <div className="product-information">
                            <div className="row">
                                
                                <div className="col-12 col-lg-6 text-center text-lg-left">
                                    <h2 className="text-light mb-0">From $699</h2>
                                    <a href="/watch" className="text-secondary">
                                        <small>Buy Now <i className="far fa-angle-right"></i></small>
                                    </a>
                                </div>

                                <div className="col-12 col-lg-5">

                                    {/* Watch Color Buttons */}
                                    <ul id="watch-toggles" className="nav py-4 pt-lg-0 justify-content-center justify-content-lg-end" role="tablist">
                                        <li>
                                            <button id="white-band-tab" className="watch-toggle" data-toggle="tab" data-target="#white-watch" role="tab">
                                                <div className="color-swatch"></div>
                                                <div className="color-title">White</div>
                                            </button>
                                        </li>
                                        <li>
                                            <button id="black-band-tab" className="watch-toggle active" data-toggle="tab" data-target="#black-watch" role="tab">
                                                <div className="color-swatch"></div>
                                                <div className="color-title">Black</div>
                                            </button>
                                        </li>
                                    </ul>

                                </div>

                            </div>
                        </div>

                    </div>
                </motion.div>

            </div>
        </div>
    </>;
}

export default Watch;
